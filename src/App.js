import React, { useState, useEffect, useCallback } from 'react'
import Loader from 'react-loader-spinner'

import APIHelper from "./APIhelper"
import TodoList from './Components/TodoList'
import Header from './Components/Header'
import Footer from './Components/Footer'

import './App.css'

function App() {

	const [todo, setTodo] = useState([])
	const [value, setValue] = useState('')
	const [activeTab, setActiveTab] = useState('All')
	const [checkAll, setCheckAll] = useState(false)
	const [show, setShow] = useState(false)
	const [editedElement, setEditedElement] = useState('')

	const [counters, setCounters] = useState({ total: 0, active: 0, done: 0 })

	const inputCb = (event) => setValue(event.target.value)

	useEffect(() => {
		const fetchTodoAndSetTodos = async () => {
			const todos = await APIHelper.getAllTodos()
			setTodo(todos)
		}
		fetchTodoAndSetTodos()
	}, [])

	// Add Todo

	const addNewTodo = async () => {
		const _value = value.trim()
		if (_value.length) {
			const newTodo = await APIHelper.createTodo(value)
			const _todo = [...todo, newTodo]
			setTodo(_todo)
			setValue('')
		}
	}

	// Add todo on Enter

	const handleKeyDown = (e) => {
		if (e.key === 'Enter') {
			addNewTodo()
		}
	}

	// Change one status

	const changeTodoStatus = async (elem) => {
		const checked = {
			done: !todo.find(todo => todo._id === elem._id).done,
		}
		const updatedTodo = await APIHelper.statusTodo(elem._id, checked)
		const changeOne = todo.map((item) => (item._id === elem._id ? updatedTodo : item))
		setTodo(changeOne)
	}

	// Change all status

	const changeAllTodoStatus = async (elem) => {
		const check = elem.target.checked
		const ids = todo.map((item) => item._id)

		await APIHelper.statusAllTodo(ids, check)

		const changeAll = todo.map((item) => ({ ...item, done: check }))
		setTodo(changeAll)
		setCheckAll(check)
	}

	// Seacrh status

	useEffect(() => {
		let validate = todo.length && todo.every((item) => item.done)
		setCheckAll(validate)
	}, [todo, value])

	// Active tab

	const getList = () => {
		return activeTab === 'Active' || activeTab === 'Complete'
			? todo.filter((el) => (activeTab === 'Complete' ? el.done : !el.done))
			: todo
	}

	const tabAll = () => {
		setActiveTab('All')
	}

	const tabActive = () => {
		setActiveTab('Active')
	}

	const tabComplete = () => {
		setActiveTab('Complete')
	}

	// Delete Todo

	const deleteTodo = async (id) => {
		await APIHelper.deleteTodo(id)
		const filteredTodo = todo.filter((item) => id !== item._id)
		setTodo(filteredTodo)
	}

	// Delete comleted Todo

	const deleteAllCompleted = async () => {

		const checkTodo = todo.filter((item) => item.done)
		const ids = checkTodo.map(item => item._id)
		await APIHelper.deleteCompletedTodo(ids)

		const filteredAllTodo = todo.filter((item) => !item.done)
		setTodo(filteredAllTodo)
	}

	// Count

	const count = useCallback(
		(_todo = todo) => {
			const result = {};
			const active = _todo.filter((item) => !item.done).length
			const total = _todo.length
			result.total = total
			result.active = active
			result.done = total - active
			setCounters(result)
		},
		[todo],
	);

	useEffect(() => {
		count(todo)
	}, [count, todo])

	// Edit

	const openEdit = (el) => {
		setEditedElement(el)
		setShow(true)
	}

	const closeEdit = async (id, value) => {
		await APIHelper.updateTodo(id, value)
		setTodo(todo.map((item) => (item._id === id ? { ...item, title: value } : item)))
		setShow(false)
	}

	return (
		<div className="todoapp" onKeyDown={handleKeyDown}>
			<Header checkAll={checkAll}
				changeAllTodoStatus={changeAllTodoStatus}
				value={value}
				addNewTodo={addNewTodo}
				inputCb={inputCb}
				setTodo={setTodo}
			/>
			{todo.length ? <TodoList
				activeTab={activeTab}
				todoTab={getList()}
				deleteTodo={deleteTodo}
				changeTodoStatus={changeTodoStatus}
				openEdit={openEdit}
				show={show}
				editedElement={editedElement}
				closeEdit={closeEdit}
			/> : <Loader
					className="loader"
					type="ThreeDots"
					color="rgba(175, 47, 47, 0.45)"
					height={60}
					width={60} />}
			<Footer
				deleteAllCompleted={deleteAllCompleted}
				tabAll={tabAll}
				tabActive={tabActive}
				tabComplete={tabComplete}
				counters={counters} />
		</div>
	)
}

export default App
