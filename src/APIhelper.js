import axios from "axios"

const API_URL = "http://localhost:3003/todos/"

async function createTodo(title) {

    const { data: newTodo } = await axios.post(API_URL, {
        title
    })
    return newTodo
}


async function deleteCompletedTodo(ids) {
    const message = await axios.delete(API_URL, {
        data: { ids }
    })
    return message
}

async function deleteTodo(id) {
    const message = await axios.delete(`${API_URL}${id}`)
    return message
}


async function statusTodo(id, payload) {
    const { data: newTodo } = await axios.put(`${API_URL}${id}`, payload)
    return newTodo
}

async function statusAllTodo(ids, check) {
    const { data: newTodo } = await axios.patch(API_URL, {
        ids, check
    })
    return newTodo
}

async function getAllTodos() {
    const { data: todos } = await axios.get(API_URL)
    return todos
}

async function updateTodo(id, value) {
    const { data: newTodo } = await axios.patch(`${API_URL}${id}`, { data: { title: value } })
    return newTodo
}

export default { createTodo, deleteTodo, statusTodo, updateTodo, getAllTodos, statusAllTodo, deleteCompletedTodo }