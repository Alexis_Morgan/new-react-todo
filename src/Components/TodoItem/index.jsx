import React, { useState } from 'react'
import Button from '../Button'
import Checkbox from '../Checkbox'
import Input from '../Input'

import './index.css'

const TodoItem = ({
  el,
  openEdit,
  closeEdit,
  deleteTodo,
  changeTodoStatus,
  show,
  editedElement,
}) => {
  const [editValue, setEditValue] = useState(el.title)

  const editInputCb = (event) => setEditValue(event.target.value)

  const _openEdit = () => {
    openEdit(el)
  }
  const _deleteTodo = () => {
    deleteTodo(el._id)
  }
  const _changeTodoStatus = (check) => {
    changeTodoStatus(el, check)
  }
  const getBlur = () => {
    closeEdit(el._id, editValue)
  }

  const enterKeyDown = (e) => {
    if (e.key === 'Enter') closeEdit(el._id, editValue)
  }
  return show && el._id === editedElement._id ? (
    <li onKeyDown={enterKeyDown}>
      <Input className="edit" value={editValue} handler={editInputCb} getBlur={getBlur} />
    </li>
  ) : (
    <li key={el._id}>
      <Checkbox className="toggle" checked={el.done} onChange={_changeTodoStatus} />
      <span className="todo-title" onDoubleClick={_openEdit}>
        {el.title}
      </span>
      <Button className="destroy" nameBtn="" onClick={_deleteTodo} />
    </li>
  )
}
export default TodoItem
