import React from 'react'
import './index.css'

const Input = ({ enter, value, placeholder, handler, className, getBlur }) => {
  return (
    <input
      className={className}
      onKeyDown={enter}
      autoFocus
      value={value}
      placeholder={placeholder}
      onChange={handler}
      onBlur={getBlur}
    />
  )
}

export default Input
