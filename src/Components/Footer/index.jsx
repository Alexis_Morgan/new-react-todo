import React from 'react'

import Button from '../Button'

const Footer = ({ tabAll, tabActive, tabComplete, deleteAllCompleted, counters }) => {
  return (
    <footer className="footer">
      <ul className="filters">
        <Button onClick={tabAll} nameBtn={`All (${counters.total})`} />
        <Button onClick={tabActive} nameBtn={`Active (${counters.active})`} />
        <Button onClick={tabComplete} nameBtn={`Complete (${counters.done})`} />
      </ul>
      <Button className="btnClear" onClick={deleteAllCompleted} nameBtn="Clear Completed" />
    </footer>
  )
}
export default Footer
