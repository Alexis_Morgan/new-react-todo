import React from 'react'

import Button from '../Button'
import Input from '../Input'
import Checkbox from '../Checkbox'

const Header = ({ checkAll, changeAllTodoStatus, value, inputCb, addNewTodo }) => {
  return (
    <header>
      <h1>todos</h1>
      <Checkbox
        id="toggle-all"
        className="toggle-all"
        checked={checkAll}
        onChange={changeAllTodoStatus}
      />
      <label htmlFor="toggle-all"></label>
      <Input
        autoFocus={true}
        placeholder="Add new todo"
        value={value}
        handler={inputCb}
        className="new-todo"
      />
      <Button className="btnAdd" onClick={addNewTodo} nameBtn="Add"></Button>
    </header>
  )
}

export default Header
