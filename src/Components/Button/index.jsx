import React from 'react'

const Button = ({ onClick, nameBtn, className }) => {
	return (
		<button className={className} onClick={onClick}>
			{nameBtn}
		</button>
	)
}

export default Button
