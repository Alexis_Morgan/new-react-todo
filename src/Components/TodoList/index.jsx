import React from 'react'
import TodoItem from '../TodoItem'

import './index.css'

const TodoList = ({
  changeTodoStatus,
  todoTab,
  deleteTodo,
  openEdit,
  closeEdit,
  show,
  editedElement,
}) => {
  return (
    <div>
      <section className="main">
        <ul className="todo-list">
          {todoTab.map((el) => (
            <TodoItem
              el={el}
              key={el._id}
              openEdit={openEdit}
              deleteTodo={deleteTodo}
              changeTodoStatus={changeTodoStatus}
              show={show}
              editedElement={editedElement}
              closeEdit={closeEdit}
            />
          ))}
        </ul>
      </section>
    </div>
  )
}

export default TodoList
